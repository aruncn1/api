# Author: Arun Mummidi
# Filename: geocode.py
# Depends: None
# Used in: ./findARestaurant.py
# 

import httplib2
import json

# getGeocodeLocation() function takes city name or any place as parameter and returns latitude and logitude information using Google Maps Geocode location API
# we will call this getGeocodeLocation from other scripts. Parameter is city/place name
def getGeocodeLocation(inputString):
    # Use Google Maps to convert a location into Latitute/Longitute coordinates
    # FORMAT: https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=API_KEY
    google_api_key = "AIzaSyCQh0eSn-lIez4-1yoKymEwTuPyFXgwcik"
    locationString = inputString.replace(" ", "+")
    url = ('https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s'% (locationString, google_api_key))
    h = httplib2.Http()
    result = json.loads(h.request(url,'GET')[1])
    latitude = result['results'][0]['geometry']['location']['lat']
    longitude = result['results'][0]['geometry']['location']['lng']
    return_string = str(latitude) + ',' + str(longitude)
    #Combine latitude and logitude into single string for ease of passing to ./findARestaurant.py
    return (return_string)
