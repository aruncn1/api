# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This project is to test the functionality of REST API. I have used Google Maps API and Foursquare API which are free to public use.

findARestaurant.py
This repo has findARestaurant.py which takes city/place name and favourite meal from the user and returns the restaurant information in json format. (Formating the Json data to HTML and user friendly is in the queue)

###findARestaurant.py depends upon geocode.py###

geocode.py
This repo has geocode.py which takes city/place name from the user, invokes an API request to Google Maps Geocode API and formats the returned data and returns latitude and longitude coordinates

* Version
I am not really familiar with the code versioning yet ;)

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
This code is written using python 2.7
You may need to have httplib2 module on your system to run the code

* Running
You can run the application by using "python findARestaurant.py"
Give all your inputs in double quotes at the moment

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Please report any bugs/feedback to 
* Arun Mummidi: aruncn1@gmail.com